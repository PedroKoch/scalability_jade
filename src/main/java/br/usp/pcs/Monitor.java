package br.usp.pcs;

import jade.core.Agent;
import jade.core.behaviours.*;
import jade.core.AID;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

import java.util.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Math;



public class Monitor extends Agent {

    List<Occurrence> occurrences = new ArrayList<Occurrence>();
    List<Occurrence> open_occurrences = new ArrayList<Occurrence>();
    private Integer received_occurrences = 0, open_occ = 0, confirmations = 0;
    private double sumTotal = 0;
    private String name = "";
    private String fileName = "";

    public void setup() {
        Object[] args = getArguments();
        if(args != null && args.length > 0) {
            String myCoveredArea = (String) args[0];
            AID id = new AID(myCoveredArea, AID.ISLOCALNAME);
        }
        name = this.getName();
        fileName = "./" + name.substring(0, name.indexOf("@")) + "_log.csv";
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false));
            writer.append("id,opened_at,ongoing_at,closed_at,total");
            writer.newLine();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addBehaviour(new Monitoring(this));
    }

    public void updateTotal(double t) {
        sumTotal += t;
    }

    public class Occurrence extends Object {
        public String id, teamName, agentName;
        public double vX, vY, teamX, teamY, opened, ongoing, closed, total = 0;
        List<Candidate> candidates = new ArrayList<Candidate>();
        public Occurrence(String ID, double posx, double posy, double ini) {
            this.agentName = getName();
            this.id = ID;
            this.vX = posx;
            this.vY = posy;
            this.opened = ini;
        }
        public void addCandidate(String name, double px, double py) {
            this.candidates.add(new Candidate(name, px, py));
        }
        public int countCandidates() {
            return this.candidates.size();
        }
        public void calculateDistances() {
            for (int i = 0; i < this.candidates.size(); i++) {
                candidates.get(i).calculateDistance(this.vX, this.vY);
            }
        }
        public void sortCandidates() {
            this.calculateDistances();
            Collections.sort(this.candidates);
        }
        public void clearCandidates() {
            this.candidates.clear();
        }
        public void printCandidates() {
            for (int i = 0 ; i < this.candidates.size() ; i++)
                System.out.println("-> " + this.candidates.get(i).name + " - " + this.candidates.get(i).dist);
        }
        public void setTeam(int i, double t) {
            this.teamName = this.candidates.get(i).name.toString();
            this.ongoing = t;
        }
        public void setFinished(double t) {
            this.closed = t;
            this.total = this.closed - this.opened;
            updateTotal(this.total);
            this.register();
        }
        public void register() {
            if(this.total > 0) {
//                String fileName = "./" + this.agentName.substring(0, this.agentName.indexOf("@")) + "_log.txt";
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
//                    writer.append(this.agentName + " - " + this.id);
//                    writer.newLine();
//                    writer.append("opened at: " + this.opened + " ; " + "ongoing at: " + this.ongoing + " ; " + "closed at: " + this.closed);
//                    writer.newLine();
//                    writer.append("Total: " + this.total);
//                    writer.newLine();
//                    writer.append("***********--------------------***********");
                    writer.append(this.id + "," + this.opened + "," + this.ongoing + "," + this.closed + "," + this.total);
                    writer.newLine();
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Error logging " + this.id + " by " + this.agentName + ".");
            }
        }
    }

    public class Candidate extends Object implements Comparable<Candidate> {
        public String name;
        public double px, py, dist;
        public Candidate(String name, double px, double py) {
            this.name = name;
            this.px = px;
            this.py = py;
        }
        public void calculateDistance(double x, double y) {
            this.dist = Round.round(Math.hypot((x-this.px), (y-this.py)), 3);
        }
        @Override
        public int compareTo(Candidate o) {
            return this.dist > o.dist ? 1 : this.dist < o.dist ? -1 : 0;
        }
    }

    public class Monitoring extends CyclicBehaviour {
        public Monitoring(Agent a) {
            super(a);
        }

        @Override
        public void action() {
            ACLMessage msg = receive();
            if(msg != null) {
                String content = msg.getContent();
                String type = content.substring(0, 1);
                //System.out.println("content : " + content + " (" + content.length() + ")");
                //System.out.println("type : " + type);
                if(msg.getPerformative() == ACLMessage.REQUEST && type.equals("A") && content.contains(";")) {
                    double vX = Double.parseDouble(content.substring(content.indexOf("X")+1, content.lastIndexOf("Y")-1));
                    double vY = Double.parseDouble(content.substring(content.indexOf("Y")+1, content.lastIndexOf(";")-1));
                    occurrences.add(new Occurrence("ID" + System.currentTimeMillis(), vX, vY, System.currentTimeMillis()));
                    received_occurrences += 1;
                    //System.out.println("received = " + received_occurrences);
                    addBehaviour(new FindNearestTeam(myAgent, occurrences.get(occurrences.size()-1)));
                } else {
                    myAgent.putBack(msg);
                }
            } else {
                block();
            }
        }
    }

    public class FindNearestTeam extends TickerBehaviour {
        private Occurrence occ;

        public FindNearestTeam(Agent a, Occurrence occ) {
            super(a, 10000);
            this.occ = occ;
        }

        protected void onTick() {
            // Procurando serviços registrados
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("maintenance");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.search(myAgent, template);
                this.occ.clearCandidates();
                if(result.length > 0) {
                    //System.out.println("Encontrados os seguintes agentes provedores do servico 'maintenance': ");
                    String maintenant;
                    for (int i = 0; i < result.length; ++i) {
                        maintenant = result[i].getName().getName();
                        int props = 0;
                        double x = 0, y = 0;
                        for (Iterator s = result[i].getAllServices(); s.hasNext(); ) {
                            ServiceDescription o = (ServiceDescription) s.next();
                            //System.out.println("s = " + o + ". ");
                            props = 0;
                            for (Iterator p = o.getAllProperties(); p.hasNext(); ) {
                                Property property = (Property) p.next();
                                if(property.getName().equals("posX")) {
                                    props += 1;
                                    x = Double.parseDouble((String) property.getValue());										}
                                if(property.getName().equals("posY")) {
                                    props += 1;
                                    y = Double.parseDouble((String) property.getValue());
                                }
                                //System.out.println("p = " + property.getName() + " - " + property.getValue() + " - " + props);
                            }
                            if(props == 2) {
                                this.occ.addCandidate(maintenant, x, y);
                            } //else //serviço disponibilizado não apresenta as duas properties necessárias
                        }
                    }
                } else {
                    System.out.println("Não foram encontrados times disponíveis.");
                }
            }
            catch (FIPAException fe) {
                fe.printStackTrace();
            }

            if(this.occ.countCandidates() > 0) {
                //System.out.println("Serviço encontrado!");
                this.occ.sortCandidates();
                myAgent.addBehaviour(new Proposal(myAgent, this.occ));
                this.stop();
            } else {
                if( this.getTickCount() % 6 == 0 && this.getTickCount() > 0 ) {
                    System.out.println(myAgent.getName() + ": Faz "+ this.getTickCount()/6 + " minutos que não encontro nenhum serviço! :(");
                }
            }
        }
    }

    public class Proposal extends SimpleBehaviour {
        private Occurrence occ;
        private int tries = 0;
        private boolean waiting = false, finished = false;

        public Proposal(Agent a, Occurrence o) {
            super(a);
            occ = o;
        }
        @Override
        public void action() {
            if(!waiting) {
                ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
                msg.addReceiver(new AID(this.occ.candidates.get(tries++).name, AID.ISGUID));
                msg.setContent("GX" + this.occ.vX + "Y" + this.occ.vY + ";");
                send(msg);
                waiting = true;
            } else {
                ACLMessage msg = receive();
                if(msg != null) {
                    String content = msg.getContent();
                    String type = content.substring(0, 1);
                    if(msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                        //recebimento da confirmação que o time irá se mobilizar para resolver a ocorrência
                        if(this.occ.candidates.get(tries-1).name.equals(msg.getSender().getName().trim())) {
                            this.occ.setTeam(/*msg.getSender().toString()*/tries-1, System.currentTimeMillis());
                        } else {
                            //falhas em registrar ongoing
                            //System.out.println("candidate: " + this.occ.candidates.get(tries-1).name);
                            //System.out.println("sender: " + msg.getSender().getName());
                        }

                    } else if(msg.getPerformative() == ACLMessage.REJECT_PROPOSAL) {
                        //o time não irá se mobilizar para resolver a ocorrência - achar o segundo time mais próximo
                        if(tries < this.occ.countCandidates()) {
                            waiting = false;
                        } else { //acabaram-se os candidatos
                            open_occurrences.add(occ);
                            open_occ++;
                            addBehaviour(new FindNearestTeam(myAgent, open_occurrences.get(open_occurrences.size()-1)));
                            finished = true;
                        }

                    } else if(msg.getPerformative() == ACLMessage.CONFIRM) {
                        //o time resolveu a ocorrência
                        this.occ.setFinished(System.currentTimeMillis());
                        confirmations++;
                    } else {
                        myAgent.putBack(msg);
                    }
                } else {
                    block();
                }
            }
        }
        @Override
        public boolean done() {
            return finished;
        }

    }

    public void takeDown() {
//        String fileName = "./" + this.getName().substring(0, this.getName().indexOf("@")) + "_log.txt";
        System.out.println("Total: " + this.sumTotal + " / " + this.received_occurrences + " rec_occ / " + this.confirmations + " conf_occ (" + this.getName() + ").");
//        try {
//            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
//            writer.append("***********----------***********----------***********");
//            writer.newLine();
//            writer.append("***********----------***********----------***********");
//            writer.newLine();
//            writer.append(this.getName() + " finished executing, dealing with " + this.received_occurrences + " occurrences.");
//            writer.newLine();
//            writer.append("Opened occurrences: " + this.open_occ);
//            writer.newLine();
//            writer.append("Confirmed occurrences: " + this.confirmations);
//            writer.newLine();
//            writer.append("Total: " + this.sumTotal);
//            writer.newLine();
//            writer.append("***********----------***********----------***********");
//            writer.newLine();
//            writer.append("***********----------***********----------***********");
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

}

