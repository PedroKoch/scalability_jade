package br.usp.pcs;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.domain.FIPAAgentManagement.Property;
import jade.core.behaviours.CyclicBehaviour;
import java.lang.Math;



public class Maintenant extends Agent {

    private boolean occupied = false;

    protected void setup() {
        DFAgentDescription dfd = criaServicoManutencao();
        try {
            DFService.register(this, dfd);
        }
        catch (FIPAException fe) {
            fe.printStackTrace();
        }
        //Behaviour para receber mensagens
        addBehaviour( new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if(msg != null && !occupied) {
                    occupied = true;
                    String content = msg.getContent();
                    String type = content.substring(0, 1);
                    //System.out.println("content : " + content + " (" + content.length() + ")");
                    //System.out.println("type : " + type);
                    if(msg.getPerformative() == ACLMessage.PROPOSE && type.equals("G") && content.contains(";")) {
                        double vX = Double.parseDouble(content.substring(content.indexOf("X")+1, content.lastIndexOf("Y")-1));
                        double vY = Double.parseDouble(content.substring(content.indexOf("Y")+1, content.lastIndexOf(";")-1));
                        //System.out.println("Indo até " + vX + "_" + vY + " resolver uma ocorrência.");
                        ACLMessage reply = msg.createReply();
                        reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                        reply.setContent("Indo até " + vX + "_" + vY + " resolver a ocorrência.");
                        myAgent.send(reply);
                        try {
                            DFService.deregister(myAgent);
                        } catch (FIPAException e) {
                            e.printStackTrace();
                        }
                        //Consome processamento - executando manutenção
                        Fibonacci.fibo((int) Round.round(Math.random() * 10 + 1, 0));
                        //confirma ocorrência resolvida
                        reply = msg.createReply();
                        reply.setPerformative(ACLMessage.CONFIRM);
                        reply.setContent("Ocorrência resolvida.");
                        myAgent.send(reply);
                        //Registra novamente um serviço - indica disponibilidade
                        DFAgentDescription dfd = criaServicoManutencao();
                        try {
                            DFService.register(myAgent, dfd);
                            occupied = false;
                        }
                        catch (FIPAException fe) {
                            fe.printStackTrace();
                        }
                    } else {
                        occupied = false;
                    }
                } else if(msg != null){
                    ACLMessage reply = msg.createReply();
                    reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
                    reply.setContent("Indisponível.");
                    myAgent.send(reply);
                } else {
                    block();
                }
            }
        });
    }

    private DFAgentDescription criaServicoManutencao() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("maintenance");
        sd.addProperties(new Property("posX", Round.round(Math.random() * World.WORLD_WIDTH + 1, 4) ));
        sd.addProperties(new Property("posY", Round.round(Math.random() * World.WORLD_HEIGHT + 1, 4) ));
        sd.setName("Team_" + getName());
        dfd.addServices(sd);
        return dfd;
    }

    protected void takeDown() {
        try {
            DFService.deregister(this);
        }
        catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

}
