package br.usp.pcs;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;


public class Master extends Agent {
    public Writer writer;
    private boolean waiting = false;
    private String[] public_ips = {"18.221.12.48", "18.220.155.164", "3.14.150.201", "18.221.156.156", "18.188.91.250"};
    private String[] private_ips = {"172.31.26.56", "172.31.31.22", "172.31.28.219", "172.31.19.174", "172.31.16.29"};
    int count = 0;
    int rcv = 1;

    private void out(String msg) {
        System.out.println(this.getName() + ": " + msg);
    }

    protected void setup() {
        out("Hello World! Greetings from your friend " + getName());
        doWait(8000);
        out("Waited 8 seconds...");

        String my_name = getLocalName();

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(my_name.concat(".txt"))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                MessageTemplate ms, mp, mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                long start = 0, end = 0, result = 0;
                if(!waiting) {
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    AID remoteAMSf = new AID("R" + rcv + "@" + private_ips[rcv-1] + ":1099/JADE", AID.ISGUID);
                    remoteAMSf.addAddresses("http://" + private_ips[rcv-1] + ":7778/acc");
                    // remoteAMSf.addAddresses("http://host" + rcv + ":7778/acc");
                    msg.addReceiver(remoteAMSf);
                    msg.setContent("Hello World!!");

                    ms = MessageTemplate.MatchSender(remoteAMSf);
                    mp = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    mt = MessageTemplate.and(ms, mp);

                    count += 1;
                    start = System.currentTimeMillis();
                    send(msg);
                    waiting = true;
                } else {
                    ACLMessage reply = myAgent.receive(mt);
                    if (reply != null) {
                        end = System.currentTimeMillis();
                        result = end - start;
                        out("Resposta: " + reply.getContent());
                        if(rcv == 5) {
                            rcv = 1;
                        } else {
                            rcv += 1;
                        }
                        try {
                            writer.write(Long.toString(result).concat(" " + getLocalName()+"\n"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        waiting = false;
                    } else {
                        block();
                    }
                }
                if (count == 20) {
                    out("Mandei 20 mensagens");
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    doDelete();
                }
            }
        });
    }
}

