package br.usp.pcs;

import jade.content.*;
import jade.core.*;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;



public class Cont extends Agent {
    static Integer QTY_MONITORS = 10;
    static Integer QTY_MAINTENANTS = 10;
    static Integer WORLD_WIDTH = 5000; //X axis
    static Integer WORLD_HEIGHT = 5000; //Y axis

    private void out(String msg) {
        System.out.println(this.getName() + ": " + msg);
    }

    public void setup() {
        Runtime runtime = Runtime.instance();
        Profile profile = new ProfileImpl();
        profile.setParameter(Profile.MAIN_HOST, "localhost");
        profile.setParameter(Profile.GUI, "true");
//        AgentContainer containerController = runtime.createMainContainer(profile);
        AgentContainer containerController = getContainerController();
        doWait(8000);

        out(getContainerController().getName()); //192.168.0.110:1099/JADE

        for(int i=1; i <= QTY_MONITORS; i++) {
            AgentController acc;
            try {
                acc = containerController.createNewAgent("monitor_" + i, "br.usp.pcs.Monitor", null);
                acc.start();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }
        for(int i=1; i <= QTY_MAINTENANTS; i++) {
            AgentController acc;
            try {
                acc = containerController.createNewAgent("main_" + i, "br.usp.pcs.Maintenant", null);
                acc.start();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }
        AgentController acc;
        try {
            acc = containerController.createNewAgent("da0", "br.usp.pcs.da", null);
            acc.start();
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }

    }

}
