package br.usp.pcs;

import java.text.SimpleDateFormat;
import java.util.Date;


import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;


public class World extends Agent {
    static Integer QTY_MONITORS = 10;
    static Integer QTY_MAINTENANTS = 100;
    static Integer WORLD_WIDTH = 5000; //X axis
    static Integer WORLD_HEIGHT = 5000; //Y axis

    public String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
    }

    private void out(String msg) {
        System.out.println("[" + getCurrentTimeStamp() + " - " + getName() + "] " + msg);
    }

    public void setup() {
//        Runtime runtime = Runtime.instance();
//        Profile profile = new ProfileImpl();
//        profile.setParameter(Profile.MAIN_HOST, "localhost");
//        profile.setParameter(Profile.GUI, "true");
//        AgentContainer containerController = runtime.createMainContainer(profile);
        AgentContainer containerController = getContainerController();
        out("Initializing agents ...");
        doWait(3000);

        for(int i=1; i <= QTY_MONITORS; i++) {
            AgentController acc;
            try {
                acc = containerController.createNewAgent("monitor_" + i, "br.usp.pcs.Monitor", null);
                acc.start();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }
        for(int i=1; i <= QTY_MAINTENANTS; i++) {
            AgentController acc;
            try {
                acc = containerController.createNewAgent("main_" + i, "br.usp.pcs.Maintenant", null);
                acc.start();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }
        AgentController acc;
        try {
            acc = containerController.createNewAgent("da0", "br.usp.pcs.da", null);
            acc.start();
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }

        out("Agents created!!!");

    }
}
