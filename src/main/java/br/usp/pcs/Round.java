package br.usp.pcs;

import java.lang.Math;

public class Round {
    static double round(double a, int d) {
        if(d >= 0 && d < 20) {
            return Math.round(a * Math.pow(10, (double) d)) / Math.pow(10, (double) d);
        } else {
            return a;
        }
    }
}
