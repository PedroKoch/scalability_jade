package br.usp.pcs;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

public class da extends Agent {
    private Integer QTY_MONITORS = World.QTY_MONITORS;
    private Integer QTY_MAINTENANTS = World.QTY_MAINTENANTS;
    private Integer WORLD_WIDTH = World.WORLD_WIDTH; //X axis
    private Integer WORLD_HEIGHT = World.WORLD_HEIGHT; //Y axis
    private Long start = 0L;
    private Long end = 0L;
    private int msgcount = 0;
    private int[] counts = new int[QTY_MONITORS+3];

    public void setup() {
        start = System.currentTimeMillis();
        addBehaviour(new TickerBehaviour(this, 100 / QTY_MONITORS) { //em média, cada agente Monitor recebe uma ocorrência a cada 100 milisegundos
            protected void onTick() {
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                double x = Math.random() * WORLD_WIDTH;
                double y = Math.random() * WORLD_HEIGHT;
                //msg.addReceiver(new AID("monitor_"+ Integer.toString((int) Round.round(Math.random() * (QTY_MONITORS - 1), 0) + 1), AID.ISLOCALNAME));
                msg.addReceiver(new AID("monitor_" + findAgent(x, y), AID.ISLOCALNAME));
                msg.setContent("AX" + Double.toString(Round.round(x, 4)) + "Y" + Double.toString(Round.round(y, 4)) + ";");
                send(msg);
                msgcount++;
                counts[findAgent(x, y)]++;
            }
        });
    }

    protected void takeDown() {
        end = System.currentTimeMillis();
        System.out.println("[" + this.getName() + "] Total:" + this.msgcount + " messages sent in " + Long.toString(end-start) + " milliseconds.");
        for(int i = 0; i < counts.length ; i ++) {
            System.out.println("counts(" + i + "): " + counts[i]);
        }
    }


    public int findAgent(double x, double y) {
        int bestx = 1, besty = QTY_MONITORS, ix = 0, jy = 0;;
        if(QTY_MONITORS < 2) {
            return 1;
        }
        for (int i = 1 ; i <= QTY_MONITORS / 2 ; i++) {
            for (int j = 1 ; j <= QTY_MONITORS / 2 ; j++) {
                if(i * j == QTY_MONITORS && Math.abs(i - j) < Math.abs(bestx - besty)) {
                    bestx = i; besty = j;
                }
            }
        }
        while(x >= 0) {
            ix++; x = x - 1.0 * WORLD_WIDTH / bestx;
        }
        while(y >= 0) {
            jy++; y = y - 1.0 * WORLD_HEIGHT / besty;
        }
        return 1 + (ix - 1) + bestx * (jy - 1);
    }
}
